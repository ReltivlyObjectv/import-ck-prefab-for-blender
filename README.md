# Import CK Prefab for Blender

Given a Creation Kit text-file export of selected references, allows you to import blend files with the same names as the editor IDs. Positions these appropriately.

This works for both interior and exterior cells, but exterior cells will not include environmental data like terrain and weather.

## Getting started

To install, just download this project as a .zip archive and install it to Blender. The current version has been confirmed to be working on Blender 3.1.1.

Once installed, you can import a cell by doing the following:
1) Open the Skyrim Creation Kit
2) Load up the cell in question 
3) From the export menu, export the cell placements
4) For each object that you wish to import into the scene, create a .blend file that contains the object (you can just [import them](https://github.com/niftools/blender_niftools_addon/releases) and save the .blend file), then save them all to the same folder with the name matching their editor id. This folder is referred to as the "pallet" folder.
5) Go to the Blender Import menu and choose the Creation Kit import option
6) Paste the local folder path for the pallet folder in the options menu
7) Select the cell placements.txt that you wish to import

Note: If you're importing exterior cells, I've found that disabling "clumsily recenter," because it removes the need to align multiple outdoor cells.

## License

This is just a fork of [Cobb's Blender Tools for Skyrim](https://www.nexusmods.com/skyrim/mods/81001?tab=description) to make it compatible with later versions of Blender. The original version was released under CC0 (Public Domain), and I'm maintaining the use of that license. :)