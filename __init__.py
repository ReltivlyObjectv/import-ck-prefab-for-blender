bl_info = {
    "name": "Cobb - Import CK Prefab",
    "author": "DavidJCobb",
    "blender": (2, 80, 0),
    "location": "File > Import-Export",
    "description": "Given a Creation Kit text-file export of selected references, allows you to import blend files with the same names as the editor IDs. Positions these appropriately.",
    "category": "Import-Export"
}

#
# README
#  - Create a "palette" folder containing the models you'll be using. 
#    Each of these models should be converted to *.BLEND files and named 
#    after the editor ID of the pieces in use (e.g. if the list has a 
#    sc_mausoleumEND02001 entry, you'll need a sc_mausoleumEND02.blend).
#
#  - Import the text file. Specify the palette folder in the options in 
#    the right pane.
#
#  - Your collection will be created and positioned with the top-level 
#    objects nested under two Empties, named Origin and Root. To center 
#    your objects on the origin and axis-align them, do this:
#
#     - Pick an object to serve as your center.
#
#     - Set Root's position to the negation of the center-object's 
#       position.
#
#     - Set Origin's yaw-rotation to the negation of the center-object's 
#       yaw.
#
#     - Apply transforms, starting at the top level and working your way 
#       down the tree.
#
 
# ===============================================================
 
import bpy
import bpy.props
from bpy_extras.io_utils import ImportHelper
from bpy.props import StringProperty, BoolProperty, EnumProperty, FloatProperty
from bpy.types import Operator
import mathutils
from math import radians
 
# ===============================================================

def position_object(obj, split_line, root, import_scale):
   obj.location.x = float(split_line[1])*import_scale
   obj.location.y = float(split_line[2])*import_scale
   obj.location.z = float(split_line[3])*import_scale
   obj.rotation_mode = 'ZYX'
   obj.rotation_euler = (radians(float(split_line[4])), radians(float(split_line[5])), radians(-float(split_line[6])))
   scale = float(split_line[7])
   if scale != 1:
      obj.scale = (scale, scale, scale)
   obj.parent = root

def create(editor_id, split_line, root, running_operator):
   print("Importing: " + editor_id)
   try:
      files = []
      with bpy.data.libraries.load(running_operator.palettepath + "/" + editor_id + ".blend") as (data_from, data_to):
          for name in data_from.objects:
              files.append({'name': name}) # use Libraries to scan for object names to import; collect them
      bpy.ops.wm.append(directory = running_operator.palettepath + "/" + editor_id + ".blend/Object/", files = files, autoselect=True) # use ops to actually append them
   except OSError:
      error_text = "Unable to find palette file: " + editor_id + ".blend"
      if running_operator.require_all_files == True:
         raise OSError(error_text)
      else:
         running_operator.report({'WARNING'}, error_text)
         if running_operator.placeholders_for_missing_files == True:
            print("   Failed! Creating placeholder...")
            placeholder = bpy.data.objects.new(running_operator.placeholderprefix + editor_id, None)
            bpy.context.scene.collection.objects.link(placeholder) 
            position_object(placeholder, split_line, root, running_operator.scale)
            print("   Finalized: " + editor_id + " AS " + placeholder.name)
         else:
            print("   Failed!")
         return
   except RuntimeError:
      raise OSError("Unable to append from palette file: " + editor_id + ".blend")
   print("   Imported!  " + editor_id)
   #
   for obj in bpy.context.selected_objects:
      if obj.parent is None:
         position_object(obj, split_line, root, running_operator.scale)
         print("   Finalized: " + editor_id + " AS " + obj.name)

def main(context, running_operator):
   if running_operator.palettepath == "":
      return False
   print("=======================================")
   print("   Processing Creation Kit prefab...   ")
   print("=======================================")
   file = open(running_operator.filepath, 'rt')
   #
   root = bpy.data.objects.new("Root", None)
   bpy.context.scene.collection.objects.link(root) 
   origin = bpy.data.objects.new("Origin", None)
   bpy.context.scene.collection.objects.link(origin) 
   root.parent = origin
   #
   new_objects = []
   for line in file:
      if line.strip(" \r\n\t\0") == "": # blank lines
         continue
      line = line.split("\t")
      line = list(filter(None, line)) # strip empty elements in case the user added extra tabs to line things up
      if line[1] == "Pos X": # CK-generated column headers
         continue
      editor_id = (line[0].strip(" \r\n\t\0"))[:-3] + running_operator.filesuffix
      create(editor_id, line, root, running_operator)
   #
   file.close()
   #
   if running_operator.clumsily_recenter:
      average_position = [0.0, 0.0, 0.0]
      first_yaw = 0.0
      count = 0
      for obj in bpy.data.objects:
         if obj.parent != root:
            continue
         if first_yaw == 0:
            first_yaw = obj.rotation_euler[2]
         count += 1
         average_position[0] += obj.location.x
         average_position[1] += obj.location.y
         average_position[2] += obj.location.z
      origin.rotation_euler[2] = -first_yaw
      root.location.x = -(average_position[0] / count)
      root.location.y = -(average_position[1] / count)
      root.location.z = -(average_position[2] / count)
 
# ===============================================================
 
class ImportCKPrefabOperator(bpy.types.Operator):
   """Import a Creation Kit prefab."""
   bl_idname = "import_scene.cobb_ckprefab"
   bl_label = "Creation Kit Prefab"
   bl_description = "Import a text export of references from the Creation Kit"

   filename_ext = ".txt"

   filepath: StringProperty(
      name="File path",
      description="Creation Kit export to open",
      default="",
      subtype='FILE_PATH'
   )
   palettepath: StringProperty(
      name="Palette folder",
      description="Folder containing your *.blend files, named after editor IDs. Subfolders will not be searched.",
      default="",
      subtype='DIR_PATH'
   )
   filesuffix: StringProperty(
      name="Filename suffix",
      description="The importer will search the palette folder for \"[editor ID][suffix].blend\".",
      default=""
   )
   require_all_files: BoolProperty(
      name="Require all files",
      description="Throw an error if any files are missing",
      default=False
   )
   placeholders_for_missing_files: BoolProperty(
      name="Use placeholders for missing files",
      description="Create placeholder objects (empties) for missing files",
      default=True
   )
   placeholderprefix: StringProperty(
      name="Placeholder prefix",
      description="Prefix for placeholder objects' names",
      default="MISSING - "
   )
   clumsily_recenter: BoolProperty(
      name="Clumsily recenter",
      description="Aligns the resulting prefab on the origin using the average of all objects' positions, and first non-zero yaw rotation found when looping over the objects. These changes are made via transforms on \"wrapper\" objects, so you'll be able to easily adjust things after the import.\n\nFor adjustments, pick a reference object to serve as your origin: the wrapper object named Origin should have its yaw set to the negation of the reference's yaw; the wrapper object named Root should have its position set to the negation of the reference's position.",
      default=True
   )
   scale: FloatProperty(
      name="Scale",
      description = "Scale", 
      default = 0.1,
      min = .01,
      max = 1000
   )

   def execute(self, context):
      main(context, self)
      return {'FINISHED'}

   def invoke(self, context, event):
      context.window_manager.fileselect_add(self)
      return {'RUNNING_MODAL'}
 
def menu_func(self, context):
   self.layout.operator(ImportCKPrefabOperator.bl_idname, text="Import Creation Kit Prefab")
 
def register():
   from bpy.utils import register_class
   register_class(ImportCKPrefabOperator)
   bpy.types.TOPBAR_MT_file_import.append(menu_func)
 
def unregister():
   from bpy.utils import unregister_class
   unregister_class(ImportCKPrefabOperator)
   bpy.types.TOPBAR_MT_file_import.remove(menu_func)
 
if __name__ == "__main__":
   register()